﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Net.Http;
using Newtonsoft.Json;
using MivaLibrary.Models;
using System.Net;

namespace MivaLibrary
{
	public class MivaProcessor
	{
		private const string AccessToken = "5173aaa69bf37c7b5f972ae5b815df2e";
		private const string SigKey = "FY5XycbPfuzHJW1sCsgzHwB2LFitZwC693JU7/hOG2Q";


		public MivaProcessor()
		{

		}

		/// <summary>
		/// Gets the list of Miva Orders from the New and Updated Queue
		/// </summary>
		/// <param name="storeCode"></param>
		/// <returns></returns>
		public static async Task<OrderResponse> GetMivaOrders(string storeCode)
		{
			var client = new HttpClient();

			ColumnFilter filter = new ColumnFilter
			{
				name = "ondemandcolumns",
				value = new string[]
				{
					"customer",
					"business_title",
					"items",
					"charges",
					"coupons",
					"discounts",
					"payments",
					"payment_data",
					"notes",
					"ship_method",
					"CustomField_Values:customfields:*"
				}
			};

			string passPhrase = "%gfS9N%!2WGn80lG";

			RequestPayload payload = new RequestPayload
			{
				Function = "Module",
				Store_Code = storeCode,
				Filter = new Filter[] { filter },
				Module_Code = "orderworkflow",
				Module_Function = "QueueOrderList_Load_Query",
				Queue_Code = "new_and_updated",
				Passphrase = passPhrase
			};

			string content = JsonConvert.SerializeObject(payload);

			string apiUri = "";
			switch (storeCode)
			{
				case "HSPS":
				case "HSC":
					apiUri = "https://henryscheinpromoshop.com/mm5/json.mvc";
					break;
				case "HSCB":
					apiUri = "https://shop.henryscheincustombranding.com/mm5/json.mvc";
					break;
				default:
					apiUri = "https://stores.wildmarketinggroup.com/mm5/json.mvc";
					break;
			}

			//Creates the has that is added to the header.
			string hmacHash = GenerateHMAC(SigKey, content);

			var request = new HttpRequestMessage()
			{
				Method = HttpMethod.Post,
				RequestUri = new Uri(apiUri),
				Headers =
				{
					{ HttpRequestHeader.Accept.ToString(), "*/*" },
					{ HttpRequestHeader.ContentType.ToString(), "application/json" },
					{ "X-Miva-API-Authorization", "MIVA-HMAC-SHA256 " + AccessToken + ":" + hmacHash }
				}
			};

			request.Content = new StringContent(content,
								Encoding.UTF8,
								"application/json");

			//Need to remove the Content - Type header and re-add without the character set indicator.
			request.Content.Headers.Remove("Content-Type");
			request.Content.Headers.Add("Content-Type", "application/json");

			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			var resp = new OrderResponse
			{
				success = false,
				order_list = null
			};

			try
			{
				var response = client.SendAsync(request).Result;
				string responseBody = await response.Content.ReadAsStringAsync();
				resp = JsonConvert.DeserializeObject<OrderResponse>(responseBody);

			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}

			return resp;
		}

		/// <summary>
		/// Pushes Miva orders from the New and Updated Queue to the Successfully Retrieved.
		/// </summary>
		/// <param name="storeCode"></param>
		/// <param name="orderNumber"></param>
		/// <returns></returns>
		public static async Task AcknowledgeMivaOrder(string storeCode, int orderNumber)
		{
			var client = new HttpClient();

			var orderIdList = new List<int> { orderNumber };

			MivaAcknowledgeRequestPayload payload = new MivaAcknowledgeRequestPayload
			{
				Function = "Module",
				Store_Code = storeCode,
				Module_Code = "orderworkflow",
				Module_Function = "OrderList_Acknowledge",
				Order_Ids = orderIdList
			};

			//Console.WriteLine("Unix Time: {0}", payload.Miva_Request_Timestamp);

			string content = JsonConvert.SerializeObject(payload);

			//Creates the has that is added to the header.
			string hmacHash = GenerateHMAC(SigKey, content);

			//Console.WriteLine("Content: {0}", content);

			string apiUri = "";
			switch (storeCode)
			{
				case "HSPS":
				case "HSC":
					apiUri = "https://henryscheinpromoshop.com/mm5/json.mvc";
					break;
				case "HSCB":
					apiUri = "https://shop.henryscheincustombranding.com/mm5/json.mvc";
					break;
				default:
					apiUri = "https://stores.wildmarketinggroup.com/mm5/json.mvc";
					break;
			}

			var request = new HttpRequestMessage()
			{
				Method = HttpMethod.Post,
				RequestUri = new Uri(apiUri),
				Headers =
				{
					{ HttpRequestHeader.Accept.ToString(), "*/*" },
					{ HttpRequestHeader.ContentType.ToString(), "application/json" },
					{ "X-Miva-API-Authorization", "MIVA-HMAC-SHA256 " + AccessToken + ":" + hmacHash }
				}
			};

			request.Content = new StringContent(content,
								Encoding.UTF8,
								"application/json");

			//Need to remove the Content - Type header and re-add without the character set indicator.
			request.Content.Headers.Remove("Content-Type");
			request.Content.Headers.Add("Content-Type", "application/json");

			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			var resp = new OrderResponse
			{
				success = false,
				order_list = null
			};

			try
			{
				var response = client.SendAsync(request).Result;
				string responseBody = await response.Content.ReadAsStringAsync();
				resp = JsonConvert.DeserializeObject<OrderResponse>(responseBody);

			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}

			//return "";
		}

		/// <summary>
		/// Updates the Inventory Count on Products in Miva
		/// </summary>
		/// <param name="storeCode"></param>
		/// <param name="sku"></param>
		/// <param name="inventoryCount"></param>
		/// <returns></returns>
		public static async Task<InventoryUpdateResponse> UpdateMivaInventory(string storeCode, string sku, int inventoryCount)
		{
			InventoryUpdateResponse success = new InventoryUpdateResponse();
			var client = new HttpClient();

			MivaProductUpdateRequestPayload payload = new MivaProductUpdateRequestPayload
			{
				Store_Code = storeCode,
				Function = "Product_Update",
				product_sku = sku,
				product_Inventory = inventoryCount
			};

			string content = JsonConvert.SerializeObject(payload);

			string hmacHash = GenerateHMAC(SigKey, content);

			string apiUri = "";
			switch (storeCode)
			{
				case "HSPS":
				case "HSC":
					apiUri = "https://henryscheinpromoshop.com/mm5/json.mvc";
					break;
				case "HSCB":
					apiUri = "https://shop.henryscheincustombranding.com/mm5/json.mvc";
					break;
				default:
					apiUri = "https://stores.wildmarketinggroup.com/mm5/json.mvc";
					break;
			}

			var request = new HttpRequestMessage()
			{
				Method = HttpMethod.Post,
				RequestUri = new Uri(apiUri),
				Headers =
				{
					{ HttpRequestHeader.Accept.ToString(), "*/*" },
					{ HttpRequestHeader.ContentType.ToString(), "application/json" },
					{ "X-Miva-API-Authorization", "MIVA-HMAC-SHA256 " + AccessToken + ":" + hmacHash }
				}
			};

			request.Content = new StringContent(content,
								Encoding.UTF8,
								"application/json");

			//Need to remove the Content - Type header and re-add without the character set indicator.
			request.Content.Headers.Remove("Content-Type");
			request.Content.Headers.Add("Content-Type", "application/json");

			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			try
			{
				var response = client.SendAsync(request).Result;
				string responseBody = await response.Content.ReadAsStringAsync();
				success = JsonConvert.DeserializeObject<InventoryUpdateResponse>(responseBody);

			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}

			return success;
		}

		/// <summary>
		/// Creates a Shipment record for the order items in Miva
		/// </summary>
		/// <param name="orderId"></param>
		/// <param name="itemIds"></param>
		/// <param name="storeCode"></param>
		/// <returns></returns>
		public static async Task<CreateShipmentResponse> CreateShipment(int orderId, List<int> itemIds, string storeCode)
		{
			//var shipResponse = new ShipmentCreateResonse();

			CreateShipmentResponse success = new CreateShipmentResponse();
			var client = new HttpClient();

			MivaCreateShipmentRequestPayload payload = new MivaCreateShipmentRequestPayload
			{
				Store_Code = storeCode,
				Function = "OrderItemList_CreateShipment",
				line_ids = itemIds,
				order_id = orderId
			};

			string content = JsonConvert.SerializeObject(payload);

			string hmacHash = GenerateHMAC(SigKey, content);

			string apiUri = "";
			switch (storeCode)
			{
				case "HSPS":
				case "HSC":
					apiUri = "https://henryscheinpromoshop.com/mm5/json.mvc";
					break;
				case "HSCB":
					apiUri = "https://shop.henryscheincustombranding.com/mm5/json.mvc";
					break;
				default:
					apiUri = "https://stores.wildmarketinggroup.com/mm5/json.mvc";
					break;
			}

			var request = new HttpRequestMessage()
			{
				Method = HttpMethod.Post,
				RequestUri = new Uri(apiUri),
				Headers =
				{
					{ HttpRequestHeader.Accept.ToString(), "*/*" },
					{ HttpRequestHeader.ContentType.ToString(), "application/json" },
					{ "X-Miva-API-Authorization", "MIVA-HMAC-SHA256 " + AccessToken + ":" + hmacHash }
				}
			};

			request.Content = new StringContent(content,
								Encoding.UTF8,
								"application/json");

			//Need to remove the Content - Type header and re-add without the character set indicator.
			request.Content.Headers.Remove("Content-Type");
			request.Content.Headers.Add("Content-Type", "application/json");

			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			try
			{
				var response = client.SendAsync(request).Result;
				string responseBody = await response.Content.ReadAsStringAsync();
				success = JsonConvert.DeserializeObject<CreateShipmentResponse>(responseBody);

			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}

			return success;
		}

		/// <summary>
		/// Updates the Shipment records with shipping carrier and tracking number.
		/// </summary>
		/// <param name="storeCode"></param>
		/// <param name="shipUpdates"></param>
		/// <returns></returns>
		public static async Task<ShipmentUpdateResponse> UpdateShipment(string storeCode, List<ShipmentUpdate> shipUpdates)
		{
			ShipmentUpdateResponse success = new ShipmentUpdateResponse();
			var client = new HttpClient();

			MivaUpdateShipmentRequestPayload payload = new MivaUpdateShipmentRequestPayload
			{
				Store_Code = storeCode,
				Function = "OrderShipmentList_Update",
				Shipment_Updates = shipUpdates
			};

			string content = JsonConvert.SerializeObject(payload);

			string hmacHash = GenerateHMAC(SigKey, content);

			string apiUri = "";
			switch (storeCode)
			{
				case "HSPS":
				case "HSC":
					apiUri = "https://henryscheinpromoshop.com/mm5/json.mvc";
					break;
				case "HSCB":
					apiUri = "https://shop.henryscheincustombranding.com/mm5/json.mvc";
					break;
				default:
					apiUri = "https://stores.wildmarketinggroup.com/mm5/json.mvc";
					break;
			}

			var request = new HttpRequestMessage()
			{
				Method = HttpMethod.Post,
				RequestUri = new Uri(apiUri),
				Headers =
				{
					{ HttpRequestHeader.Accept.ToString(), "*/*" },
					{ HttpRequestHeader.ContentType.ToString(), "application/json" },
					{ "X-Miva-API-Authorization", "MIVA-HMAC-SHA256 " + AccessToken + ":" + hmacHash }
				}
			};

			request.Content = new StringContent(content,
								Encoding.UTF8,
								"application/json");

			//Need to remove the Content - Type header and re-add without the character set indicator.
			request.Content.Headers.Remove("Content-Type");
			request.Content.Headers.Add("Content-Type", "application/json");

			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			try
			{
				var response = client.SendAsync(request).Result;
				string responseBody = await response.Content.ReadAsStringAsync();
				success = JsonConvert.DeserializeObject<ShipmentUpdateResponse>(responseBody);

			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}

			return success;
		}

		/// <summary>
		/// Gets the list of items in a Miva order.
		/// </summary>
		/// <param name="orderId"></param>
		/// <param name="storeCode"></param>
		/// <returns></returns>
		public static async Task<List<MivaItem>> GetOrderItems(string orderId, string storeCode)
		{
			var client = new HttpClient();

			ColumnFilter filter = new ColumnFilter
			{
				name = "ondemandcolumns",
				value = new string[]
				{
					"items"
				}
			};

			SearchFilterValue filterVal = new SearchFilterValue
			{
				field = "id",
				@operator = "EQ",
				value = orderId.ToString()
			};

			SearchFilter searchFilter = new SearchFilter
			{
				name = "search",
				value = new SearchFilterValue[]
				{
					filterVal
				}
			};


			RequestPayload payload = new RequestPayload
			{
				Function = "OrderList_Load_Query",
				Store_Code = storeCode,
				Filter = new Filter[] { filter, searchFilter }
			};

			string content = JsonConvert.SerializeObject(payload);

			//Creates the has that is added to the header.
			string hmacHash = GenerateHMAC(SigKey, content);

			string apiUri = "";
			switch (storeCode)
			{
				case "HSPS":
				case "HSC":
					apiUri = "https://henryscheinpromoshop.com/mm5/json.mvc";
					break;
				case "HSCB":
					apiUri = "https://shop.henryscheincustombranding.com/mm5/json.mvc";
					break;
				default:
					apiUri = "https://stores.wildmarketinggroup.com/mm5/json.mvc";
					break;
			}

			var request = new HttpRequestMessage()
			{
				Method = HttpMethod.Post,
				RequestUri = new Uri(apiUri),
				Headers =
				{
					{ HttpRequestHeader.Accept.ToString(), "*/*" },
					{ HttpRequestHeader.ContentType.ToString(), "application/json" },
					{ "X-Miva-API-Authorization", "MIVA-HMAC-SHA256 " + AccessToken + ":" + hmacHash }
				}
			};

			request.Content = new StringContent(content,
								Encoding.UTF8,
								"application/json");

			//Need to remove the Content - Type header and re-add without the character set indicator.
			request.Content.Headers.Remove("Content-Type");
			request.Content.Headers.Add("Content-Type", "application/json");

			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			var resp = new OrderResponse
			{
				success = false,
				order_list = null
			};

			var orderList = new List<MivaItem>();

			try
			{
				var response = client.SendAsync(request).Result;
				string responseBody = await response.Content.ReadAsStringAsync();
				resp = JsonConvert.DeserializeObject<OrderResponse>(responseBody);

				orderList = resp.order_list.orders.FirstOrDefault().items;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}

			return orderList;
		}

		public static async Task<List<MivaOrder>> GetOrders(string orderId, string storeCode)
		{
			var client = new HttpClient();

			ColumnFilter filter = new ColumnFilter
			{
				name = "ondemandcolumns",
				value = new string[]
				{
					"customer",
					"business_title",
					"items",
					"charges",
					"coupons",
					"discounts",
					"payments",
					"payment_data",
					"notes",
					"ship_method",
					"CustomField_Values:customfields:*" 
				}
			};

			SearchFilterValue filterVal = new SearchFilterValue
			{
				field = "id",
				@operator = "EQ",
				value = orderId.ToString()
			};

			SearchFilter searchFilter = new SearchFilter
			{
				name = "search",
				value = new SearchFilterValue[]
				{
					filterVal
				}
			};

			RequestPayload payload = new RequestPayload
			{
				Function = "OrderList_Load_Query",
				Store_Code = storeCode,
				Filter = new Filter[] { filter, searchFilter },
				Passphrase = "%gfS9N%!2WGn80lG"
			};

			string content = JsonConvert.SerializeObject(payload);

			//Creates the has that is added to the header.
			string hmacHash = GenerateHMAC(SigKey, content);

			string apiUri = "";
			switch (storeCode)
			{
				case "HSPS":
				case "HSC":
					apiUri = "https://henryscheinpromoshop.com/mm5/json.mvc";
					break;
				case "HSCB":
					apiUri = "https://shop.henryscheincustombranding.com/mm5/json.mvc";
					break;
				default:
					apiUri = "https://stores.wildmarketinggroup.com/mm5/json.mvc";
					break;
			}

			var request = new HttpRequestMessage()
			{
				Method = HttpMethod.Post,
				RequestUri = new Uri(apiUri),
				Headers =
				{
					{ HttpRequestHeader.Accept.ToString(), "*/*" },
					{ HttpRequestHeader.ContentType.ToString(), "application/json" },
					{ "X-Miva-API-Authorization", "MIVA-HMAC-SHA256 " + AccessToken + ":" + hmacHash }
				}
			};

			request.Content = new StringContent(content,
								Encoding.UTF8,
								"application/json");

			//Need to remove the Content - Type header and re-add without the character set indicator.
			request.Content.Headers.Remove("Content-Type");
			request.Content.Headers.Add("Content-Type", "application/json");

			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			var resp = new OrderResponse
			{
				success = false,
				order_list = null
			};

			var orderList = new List<MivaOrder>();

			try
			{
				var response = client.SendAsync(request).Result;
				string responseBody = await response.Content.ReadAsStringAsync();
				resp = JsonConvert.DeserializeObject<OrderResponse>(responseBody);

				orderList = resp.order_list.orders;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}

			return orderList;
		}

		/// <summary>
		/// Generates the HMAC string to be used to pass the encrypted message to the Miva API
		/// </summary>
		/// <param name="key"></param>
		/// <param name="message"></param>
		/// <returns></returns>
		private static string GenerateHMAC(string key, string message)
		{
			String hash = string.Empty;

			if (key.Length % 4 != 0)
			{
				key = key + new String('=', (4 - (key.Length % 4)));
			}

			byte[] keybytes = System.Convert.FromBase64String(key);

			var hmacsha256 = new System.Security.Cryptography.HMACSHA256(keybytes);

			byte[] hashmessage = hmacsha256.ComputeHash(System.Text.Encoding.ASCII.GetBytes(message));
			hash = Convert.ToBase64String(hashmessage);

			return hash;
		}
	}
}

