﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaAcknowledgeRequestPayload : RequestPayload
	{
		public List<int> Order_Ids { get; set; }		
	}
}
