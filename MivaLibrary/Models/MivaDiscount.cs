﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaDiscount
	{
		public int? order_id { get; set; }
		public int? line_id { get; set; }
		public int? attr_id { get; set; }
		public int? attmpat_id { get; set; }
		public int? pgrp_id { get; set; }
		public bool? display { get; set; }
		public string descrip { get; set; }
		public decimal? discount { get; set; }
		public decimal total { get; set; }
	}
}
