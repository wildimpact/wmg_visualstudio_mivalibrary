﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaModule
	{
		public int id { get; set; }
		public string code { get; set; }
		public string name { get; set; }
		public string provider { get; set; }
		public string api_ver { get; set; }
		public string version { get; set; }
		public string module { get; set; }
		public int refcount { get; set; }
		public bool active { get; set; }
	}
}
