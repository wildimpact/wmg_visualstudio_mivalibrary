﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class CreateShipmentResponse : MivaResponse
	{
		public ShipmentResponseData data { get; set; }	
	}
}
