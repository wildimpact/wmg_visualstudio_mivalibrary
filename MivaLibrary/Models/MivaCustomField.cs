﻿using System;

namespace MivaLibrary.Models
{
	public class MivaCustomField
	{
		public string ocst_news { get; set; }
        public string ocst_email { get; set; }
        public string ocst_name { get; set; }
        public string ocst_notes { get; set; }
        public string ocst_phone { get; set; }
        public string ocst_shipComplete { get; set; }
        public string osel_shipComplete { get; set; }
        public string ocst_dealerNumber { get; set; }
        public string osel_UPSAccountNumber { get; set; }
        public string ocst_franchiseeName { get; set; }
        public string dealerNumber { get; set; }
        public string resaleTaxID { get; set; }
        public string ocst_retailerName { get; set; }
        public DateTime osel_date { get; set; }
        public string ocst_confirmationEmail { get; set; }
        public string ocst_accountNumber { get; set; }
        public string costCenter { get; set; }
    }
}