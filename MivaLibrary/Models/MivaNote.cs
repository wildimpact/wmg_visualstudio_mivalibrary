﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaNote
	{
		public int id { get; set; }
		public int cust_id { get; set; }
		public int account_id { get; set; }
		public int order_id { get; set; }
		public int user_id { get; set; }
		public string notetext { get; set; }
		public string cust_login { get; set; }
		public string business_title { get; set; }
		public string admin_user { get; set; }
	}
}
