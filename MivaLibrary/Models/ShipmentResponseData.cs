﻿namespace MivaLibrary.Models
{
	public class ShipmentResponseData
	{
		public int id { get; set; }
		public string code { get; set; }
		public int batch_id { get; set; }
		public int order_id { get; set; }
		public int status { get; set; }
		public int labelcount { get; set; }
		public int shipdate { get; set; }
		public string tracknum { get; set; }
		public string tracktype { get; set; }
		public string tracklink { get; set; }
		public decimal weight { get; set; }
		public decimal cost { get; set; }
		public string formatted_cost { get; set; }
		public int shipment_id { get; set; }
	}
}