﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaCustomer
	{
		public int id { get; set; }
		public int account_id { get; set; }
		public string login { get; set; }
		public string pw_email { get; set; }
		public int ship_id { get; set; }
		public bool ship_res { get; set; }
		public string ship_fname { get; set; }
		public string ship_lname { get; set; }
		public string ship_email { get; set; }
		public string ship_comp { get; set; }
		public string ship_phone { get; set; }
		public string ship_fax { get; set; }
		public string ship_addr1 { get; set; }
		public string ship_addr2 { get; set; }
		public string ship_city { get; set; }
		public string ship_state { get; set; }
		public string ship_zip { get; set; }
		public string ship_cntry { get; set; }
		public string bill_fname { get; set; }
		public string bill_lname { get; set; }
		public string bill_email { get; set; }
		public string bill_comp { get; set; }
		public string bill_phone { get; set; }
		public string bill_fax { get; set; }
		public string bill_addr1 { get; set; }
		public string bill_addr2 { get; set; }
		public string bill_city { get; set; }
		public string bill_state { get; set; }
		public string bill_zip { get; set; }
		public string bill_cntry { get; set; }
		public bool tax_exempt { get; set; }
		public int note_count { get; set; }
		public int dt_created { get; set; } // Unix timestamp
		public int dt_login { get; set; }
		public decimal credit { get; set; }
		public string formatted_credit { get; set; }
        public string business_title { get; set; }
    }
}
