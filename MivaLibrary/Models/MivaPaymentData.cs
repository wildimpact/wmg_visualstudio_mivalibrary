﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaPaymentData
	{
		public string check { get; set; }
		public string cc_card { get; set; }
		public string cc_name { get; set; }
		public string cc_number { get; set; }
		public string cc_exp { get; set; }
		public string poplus { get; set; }
	}
}
