﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaCoupon
	{
		public int order_id { get; set; }
		public int coupon_id { get; set; }
		public string code { get; set; }
		public string descrip { get; set; }
		public decimal total { get; set; }
	}
}
