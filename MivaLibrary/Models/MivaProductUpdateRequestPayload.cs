﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaProductUpdateRequestPayload : RequestPayload
	{
		public string product_sku { get; set; }
		public int product_Inventory { get; set; }
	}
}
