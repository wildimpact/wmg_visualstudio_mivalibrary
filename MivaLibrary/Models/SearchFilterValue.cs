﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class SearchFilterValue
	{
		public string field { get; set; }
		[JsonProperty("operator")]
		public string @operator { get; set; }
		public string value { get; set; }
	}
}
