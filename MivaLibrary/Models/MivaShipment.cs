﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaShipment
	{
		public int id { get; set; }
		public string code { get; set; }
		public int order_id { get; set; }
		public int status { get; set; }
		public int labelcount { get; set; }
		public int ship_date { get; set; }
		public string tracknum { get; set; }
		public string tracktype { get; set; }
		public string tracklink { get; set; }
		public decimal weight { get; set; }
		public decimal cost { get; set; }
		public string formatted_cost { get; set; }
	}
}
