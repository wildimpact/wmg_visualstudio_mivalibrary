﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaProductSubscription
	{
		public int? id { get; set; }
		public int? product_id { get; set; }
		public int? frequency { get; set; }
		public int? term { get; set; }
		public string descrip { get; set; }
		public int? n { get; set; }
		public int? fixed_dow { get; set; }
		public int? fixed_dom { get; set; }
		public int? sub_count { get; set; }
	}
}
