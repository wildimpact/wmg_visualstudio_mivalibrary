﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MivaLibrary.Models
{
	public class OrderResponse : MivaResponse
	{		
		[JsonProperty("data")]
		public OrderList order_list { get; set; }
	}
}
