﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaOptions
	{
		public string attribute { get; set; }
		public string value { get; set; }
		public decimal weight { get; set; }
		public decimal retail { get; set; }
		public decimal base_price { get; set; }
		public decimal price { get; set; }
		public List<MivaDiscount> discounts { get; set; }
	}
}
