﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class ShipmentUpdate
	{
		public int shpmnt_id { get; set; }
		public bool mark_shipped { get; set; }
		public string tracknum { get; set; }
		public string tracktype { get; set; }		
		public decimal cost { get; set; }
		public decimal weight { get; set; }
	}
}
