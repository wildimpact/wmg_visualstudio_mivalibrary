﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaCreateShipmentRequestPayload : RequestPayload
	{
		public int order_id { get; set; }
		public List<int> line_ids { get; set; }
	}
}
