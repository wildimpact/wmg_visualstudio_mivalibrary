﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaItem
	{
		public int order_id { get; set; }
		public int line_id { get; set; }
		public int status { get; set; }
		public int subscrp_id { get; set; }
		public int subterm_id { get; set; }
		public int rma_id { get; set; }
		public int dt_instock { get; set; } //Unix Timestamp
		public string code { get; set; }
		public string name { get; set; }
		public string sku { get; set; }
		public decimal retail { get; set; }
		public decimal base_price { get; set; }
		public decimal price { get; set; }
		public decimal weight { get; set; }
		public bool taxable { get; set; }
		public bool upsold { get; set; }
		public int quantity { get; set; }
		public decimal total { get; set; }
		public List<MivaDiscount> discounts { get; set; }
		public List<MivaParts> parts { get; set; }
		public List<MivaOptions> options { get; set; }
		public MivaShipment shipment { get; set; }
		public MivaSubscription subscription { get; set; }
		public List<MivaCoupon> coupons { get; set; }
		public List<MivaNote> notes { get; set; }
	}
}
