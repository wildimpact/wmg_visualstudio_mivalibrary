﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaCharge
	{
		public int order_id { get; set; }
		public int charge_id { get; set; }
		public int module_id { get; set; }
		public string type { get; set; }
		public string descrip { get; set; }
		public decimal amount { get; set; }
		public decimal disp_amt { get; set; }
		public bool tax_exempt { get; set; }
	}
}
