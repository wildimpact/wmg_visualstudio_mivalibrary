﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaSubscription
	{
		public int? id { get; set; }
		public int? order_id { get; set; }
		public int? line_id { get; set; }
		public int? cust_id { get; set; }
		public int? custpc_id { get; set; }
		public int? product_id { get; set; }
		public int? subterm_id { get; set; }
		public int? addr_id { get; set; }
		public int? ship_id { get; set; }
		public string ship_data { get; set; }
		public int? quantity { get; set; }
		public int? termrem { get; set; }
		public int? termproc { get; set; }
		public int? firstdate { get; set; }
		public int? lastdate { get; set; }
		public int? nextdate { get; set; }
		public string status { get; set; }
		public string message { get; set; }
		public int? cncldate { get; set; }
		public decimal? tax { get; set; }
		public string formatted_tax { get; set; }
		public decimal? shipping { get; set; }
		public string formatted_shipping { get; set; }
		public decimal? subtotal { get; set; }
		public string formatted_subtotal { get; set; }
		public decimal? total { get; set; }
		public string method { get; set; }
		public MivaProductSubscription productsubscriptionterm { get; set; }
	}
}
