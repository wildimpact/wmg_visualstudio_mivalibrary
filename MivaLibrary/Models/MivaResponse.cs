﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaResponse
	{
		public bool success { get; set; }
		public string error_code { get; set; }
		public string error_message { get; set; }
	}
}
