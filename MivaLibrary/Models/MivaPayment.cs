﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MivaLibrary.Models
{
	public class MivaPayment
	{
		public int id { get; set; }
		public int order_id { get; set; }
		public int type { get; set; }
		public string refnum { get; set; }
		public decimal amount { get; set; }
		public string formatted_amount { get; set; }
		public decimal available { get; set; }
		public string formatted_available { get; set; }
		public string dtstamp { get; set; }
		public string expires { get; set; }
		public int pay_id { get; set; }
		public int pay_secid { get; set; }
		public string decrypt_status { get; set; }
		public string description { get; set; }
		[JsonProperty("module")]
		public MivaModule module { get; set; }
		[JsonProperty("data")]
		public MivaPaymentData data { get; set; }
	}
}
