﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MivaLibrary.Models
{
	public class MivaOrder
	{
		public int id { get; set; }
		public int pay_id { get; set; }
		public int batch_id { get; set; }
		public int status { get; set; }
		public int pay_status { get; set; }
		public int stk_status { get; set; }
		public int orderdate { get; set; } // Unix timestamp
		public int cust_id { get; set; }
		public bool ship_res { get; set; }
		public string ship_fname { get; set; }
		public string ship_lname { get; set; }
		public string ship_email { get; set; }
		public string ship_comp { get; set; }
		public string ship_phone { get; set; }
		public string ship_fax { get; set; }
		public string ship_addr1 { get; set; }
		public string ship_addr2 { get; set; }
		public string ship_city { get; set; }
		public string ship_state { get; set; }
		public string ship_zip { get; set; }
		public string ship_cntry { get; set; }
		public string bill_fname { get; set; }
		public string bill_lname { get; set; }
		public string bill_email { get; set; }
		public string bill_comp { get; set; }
		public string bill_phone { get; set; }
		public string bill_fax { get; set; }
		public string bill_addr1 { get; set; }
		public string bill_addr2 { get; set; }
		public string bill_city { get; set; }
		public string bill_state { get; set; }
		public string bill_zip { get; set; }
		public string bill_cntry { get; set; }
		public int ship_id { get; set; }
		public string ship_data { get; set; }
		public string ship_method { get; set; }
		public string source { get; set; }
		public int source_id { get; set; }
		public decimal total { get; set; }
		public string formatted_total { get; set; }
		public decimal total_ship { get; set; }
		public string formatted_total_ship { get; set; }
		public decimal total_tax { get; set; }
		public string formatted_total_tax { get; set; }
		public decimal total_auth { get; set; }
		public string formatted_total_auth { get; set; }
		public decimal total_rfnd { get; set; }
		public string formatted_total_rfnd { get; set; }
		public decimal net_capt { get; set; }
		public string formatted_net_capt { get; set; }
		public int pend_count { get; set; }
		public int bord_count { get; set; }
		public int note_count { get; set; }
        public string business_title { get; set; }
        [JsonProperty("customer")]
		public MivaCustomer customer { get; set; }
		[JsonProperty("items")]
		public List<MivaItem> items { get; set; }
		[JsonProperty("charges")]
		public List<MivaCharge> charges { get; set; }
		[JsonProperty("payments")]
		public List<MivaPayment> payments { get; set; }
		[JsonProperty("discounts")]
		public List<MivaPayment> discounts { get; set; }
		[JsonProperty("coupons")]
		public List<MivaCoupon> coupons { get; set; }
		[JsonProperty("CustomField_Values")]
		public MivaCustomFieldValues CustomField_Values { get; set; }
	}
}
