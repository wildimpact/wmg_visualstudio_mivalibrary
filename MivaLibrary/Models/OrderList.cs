﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MivaLibrary.Models
{
	public class OrderList
	{
		public int total_count { get; set; }
		public int start_offset { get; set; }
		[JsonProperty("data")]
		public List<MivaOrder> orders { get; set; }
	}
}
