﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
    public class MivaUpdateShipmentRequestPayload : RequestPayload
    {
        public List<ShipmentUpdate> Shipment_Updates { get; set; }
    }

}
