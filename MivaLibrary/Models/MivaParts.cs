﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class MivaParts
	{
		public string code { get; set; }
		public string sku { get; set; }
		public string name { get; set; }
		public int? quantity { get; set; }
		public int? total_quantity { get; set; }
		public decimal? price { get; set; }
	}
}
