﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MivaLibrary.Models
{
	public class RequestPayload
	{
		public string Function { get; set; }
		public string Store_Code { get; set; }
		public string Miva_Request_Timestamp => ((long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0))).TotalSeconds).ToString();
		public Filter[] Filter { get; set; }
		public string Passphrase { get; set; }
		public string Module_Code { get; set; }
		public string Module_Function { get; set; }
		public string Queue_Code { get; set; }
	}
}
