﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MivaLibrary;
using MivaLibrary.Models;
using NUnit.Framework;

namespace MivaLibrary.Tests
{
	[TestFixture]
    public class MivaProcessorTests
    {
		[Test]
		public void CanUpdateInventory()
		{
			var invresp = new InventoryUpdateResponse();

			invresp = MivaProcessor.UpdateMivaInventory("MATH", "MATH168", 10).GetAwaiter().GetResult();

			Assert.IsTrue(invresp.success);
		}

		[Test]
		public void GetOrderItems()
		{
			var orderResp = new List<MivaItem>();
			string orderId = "1027";
			string storeCode = "SNPDE";

			orderResp = MivaProcessor.GetOrderItems(orderId, storeCode).GetAwaiter().GetResult();

			Assert.IsTrue(orderResp.Count > 0);
			Assert.IsTrue(orderResp.All(o => o.order_id == Convert.ToInt32(orderId)));
		}

		[Test]
		public void CreateShipment()
		{
			var shipResponse = new CreateShipmentResponse();
			List<int> itemIds = new List<int> { 50 };

			shipResponse = MivaProcessor.CreateShipment(1017, itemIds, "SB").GetAwaiter().GetResult();

			Assert.IsTrue(shipResponse.success == true);
		}

		[Test]
		public void UpdateShipment()
		{
            var shipUdate = new ShipmentUpdate
            {
                mark_shipped = true,
                shpmnt_id = 5,
                tracknum = "1234",
                tracktype = "UPS"
            };

            List<ShipmentUpdate> shipUpdtList = new List<ShipmentUpdate>();
            shipUpdtList.Add(shipUdate);

            var shipUpdtResponse = new ShipmentUpdateResponse();

            shipUpdtResponse = MivaProcessor.UpdateShipment("SB", shipUpdtList).GetAwaiter().GetResult();

            Assert.IsTrue(shipUpdtResponse.success == true);
		}

    }
}
